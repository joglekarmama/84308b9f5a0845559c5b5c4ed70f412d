import React from 'react';
import config from 'visual-config-exposer';
import _ from 'lodash';

import './socialLinks.css';

const socialLinks = config.settings.socialLinks;

const SocialLinks = () => {
  const socialLinksArray = [];

  for (const key of Object.keys(socialLinks)) {
    socialLinksArray.push(socialLinks[key]);
  }

  const linksCollection = socialLinksArray.map((link) => ({
    name: link.platform,
    link: link.platformLink,
    order: link.order,
    image: link.platformImg,
  }));
  console.log(linksCollection);

  const sortedLinksArray = _.orderBy(linksCollection, ['order'], ['asc']);

  const linkJsx = sortedLinksArray.map((link) => (
    <div className="social_link-container" key={link.name}>
      <a className="social_link" href={link.link}>
        <div
          className="social_image"
          style={{ backgroundImage: `url(${link.image})` }}
        ></div>
      </a>
    </div>
  ));

  return <div className="social_links-container">{linkJsx}</div>;
};

export default SocialLinks;
